# -*- coding: utf-8 -*-
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterNumber,
                       QgsWkbTypes,
                       QgsConstWkbPtr,
                       QgsLineString,
                       QgsPolygon,
                       QgsMultiLineString,
                       QgsPoint,
                       QgsGeometry,
                       QgsFields,
                       QgsField,
                       QgsFeature,
                       QgsFeatureRequest
                       )
from qgis import processing
from collections import deque
from math import sqrt


def get_tr_segment(value):
    return value["TR_SEGMENT"]


class InterpolateRivers(QgsProcessingAlgorithm):
    INPUT_POLY = 'INPUT_POLY'
    INPUT_LINES = 'INPUT_LINES'
    OUTPUT = 'OUTPUT'
    TRANSECT_LENGTH = 'TRANSECT_LENGTH'

    @staticmethod
    def tr(text):
        return QCoreApplication.translate('Processing', text)

    def createInstance(self):
        return InterpolateRivers()

    def name(self):
        return 'interpolateRivers'

    def displayName(self):
        return self.tr('Interpolate Rivers')

    def group(self):
        return self.tr('Water Leveling')

    def groupId(self):
        return 'levelingwater'

    def shortHelpString(self):
        return self.tr("This script interpolate rivers")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_POLY,
                self.tr('Input layer with polygons of rivers'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LINES,
                self.tr('Input layer with rivers'),
                [QgsProcessing.TypeVectorLine]
            )
        )

        param_transect_length = QgsProcessingParameterNumber(
            self.TRANSECT_LENGTH,
            self.tr('Transect length'),
            QgsProcessingParameterNumber.Double
        )

        param_transect_length.setMetadata({'widget_wrapper': {'decimals': 0}})
        self.addParameter(param_transect_length)

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer'),
                QgsProcessing.TypeVectorAnyGeometry
            )
        )

    def getSource(self, parameters, context, parameter_name):
        source = self.parameterAsSource(parameters, parameter_name, context)
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, parameter_name))
        return source

    @staticmethod
    def transect(transected_layer, transect_length):
        return processing.run("native:transect",
                              {'INPUT': transected_layer, 'LENGTH': transect_length, 'SIDE': 2,
                               'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    @staticmethod
    def clip(input_layer, overlay_layer):
        return processing.run("qgis:clip",
                              {'INPUT': input_layer, 'OVERLAY': overlay_layer, 'OUTPUT': 'TEMPORARY_OUTPUT'})['OUTPUT']

    @staticmethod
    def addPolygon(curr_polygon_points, river_id, sink):
        polygon = QgsGeometry(QgsPolygon(QgsLineString(list(curr_polygon_points))))
        fields = QgsFields()
        fields.append(QgsField('river_id', QVariant.Int, 'int'))
        polygon_feature = QgsFeature(fields)
        polygon_feature.setGeometry(polygon)
        polygon_feature['river_id'] = river_id
        sink.addFeature(polygon_feature, QgsFeatureSink.FastInsert)

    @staticmethod
    def distance(point1, point2):
        return sqrt((point1.x() - point2.x()) ** 2 + (point1.y() - point2.y()) ** 2)

    def selectNearestLine(self, multiline, point):
        min_diff = None
        nearest_line = None
        for line in multiline:
            distance_left = self.distance(line[0], point)
            distance_right = self.distance(line[1], point)
            sum_distance = distance_left + distance_right
            line_length = self.distance(line[0], line[1])
            if min_diff is None or abs(line_length - sum_distance) < min_diff:
                min_diff = abs(line_length - sum_distance)
                nearest_line = line
        return nearest_line

    @staticmethod
    def selectLinesByRiverId(river_id, lines_source):
        featureList = lines_source.getFeatures(QgsFeatureRequest().setFilterExpression('"river_id"=' + str(river_id)))
        features = list()
        for feature in featureList:
            features.append(feature)
        features.sort(key=get_tr_segment)
        return features

    def interpolateRiversIndexes(self, clipped_transected_lines, sink, rivers_points, river_ids, feedback):
        total = 100.0 / (clipped_transected_lines.featureCount()) if clipped_transected_lines.featureCount() else 0
        count_steps = 0
        polygon_points = deque()
        for river_id in river_ids:
            for clipped_transected_line in self.selectLinesByRiverId(river_id, clipped_transected_lines):
                if feedback.isCanceled():
                    return
                point_index = clipped_transected_line["TR_SEGMENT"] - 1
                side_points = self.selectNearestLine(clipped_transected_line.geometry().asMultiPolyline(),
                                                     rivers_points[river_id][point_index])
                polygon_points.append(
                    QgsPoint(side_points[1].x(), side_points[1].y(), rivers_points[river_id][point_index].z()))
                polygon_points.appendleft(
                    QgsPoint(side_points[0].x(), side_points[0].y(), rivers_points[river_id][point_index].z()))
                count_steps += 1
                feedback.setProgress(int(count_steps * total))
            self.addPolygon(polygon_points, river_id, sink)
            polygon_points.clear()

    @staticmethod
    def readRivers(river_lines):
        rivers_points, river_ids = dict(), list()
        for line in river_lines:
            heights, points = list(), list()
            multiLineZWkb = line.geometry().asWkb()
            multiLineZ = QgsMultiLineString()
            multiLineZ.fromWkb(QgsConstWkbPtr(multiLineZWkb))
            for point_list in multiLineZ:
                for point in point_list:
                    heights.append(point.z())
                    points.append(point)
            rivers_points[line["river_id"]] = points
            river_ids.append(line["river_id"])
        return rivers_points, river_ids

    def getSink(self, parameters, context, src):
        sink_fields = QgsFields()
        sink_fields.append(QgsField('river_id', QVariant.Int, 'int'))
        sink, dest_id = self.parameterAsSink(parameters, self.OUTPUT, context, sink_fields, QgsWkbTypes.MultiPolygonZ,
                                             src)
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))
        return sink, dest_id

    def processAlgorithm(self, parameters, context, feedback):
        clipped_lines = self.clip(parameters[self.INPUT_LINES], parameters[self.INPUT_POLY])
        rivers_points, river_ids = self.readRivers(clipped_lines.getFeatures())
        transect_length = self.parameterAsDouble(parameters, self.TRANSECT_LENGTH, context)
        transected_lines = self.transect(clipped_lines, transect_length)
        clipped_transected_lines = self.clip(transected_lines, parameters[self.INPUT_POLY])
        sink, dest_id = self.getSink(parameters, context, clipped_transected_lines.sourceCrs())
        self.interpolateRiversIndexes(clipped_transected_lines, sink, rivers_points, river_ids, feedback)
        return {self.OUTPUT: dest_id}

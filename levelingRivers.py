# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterVectorLayer,
                       QgsGeometry,
                       QgsField,
                       QgsPoint,
                       QgsFields,
                       QgsFeature,
                       QgsWkbTypes)
from qgis import processing
import tempfile


def pip_install(module_name):
    try:
        import pip
    except ImportError:
        raise Exception(
            "You have to install " + module_name + ": python3 -m pip install " + module_name + ". Or update your python version (3.4 or higher)")
    pip.main(['install', module_name])


try:
    import numpy as np
except ImportError:
    pip_install("numpy")
    import numpy as np

try:
    from scipy import interpolate
except ImportError:
    pip_install("scipy")
    from scipy import interpolate


MIN_GREATEST_DIF = 0.2


def create_temp_file():
    temp = tempfile.NamedTemporaryFile()
    return temp.name


def parse_name(uri):
    params = uri.split("|")
    return params[0]


def get_anomaly_val(list_values):
    diffs = np.zeros(len(list_values) - 1)
    for i in range(len(list_values) - 1):
        diffs[i] = abs(list_values[i + 1] - list_values[i])
    sort_diffs = np.sort(diffs)
    return sort_diffs[int(len(sort_diffs) * 0.8)]


def draw_trend(list_values):
    if len(list_values) == 0:
        return np.poly1d(0)
    z = np.polyfit(np.arange(0, len(list_values)), list_values, 1)
    p = np.poly1d(z)
    return p  # it is the function


def make_inclined(list_values, start_pos, end_pos, trend, is_shot):
    for i in range(start_pos, end_pos + 1):
        list_values[i] = trend(i)
        is_shot[i] = True


def shot_inclined(list_values, start_pos, to_right, anomaly_points, trend, is_shot):
    if to_right and list_values[start_pos] < list_values[start_pos + 1]:
        for i in range(start_pos + 1, len(list_values)):
            if (list_values[i] < list_values[start_pos] and i not in anomaly_points) or (i == len(list_values) - 1) or \
                    is_shot[i]:
                make_inclined(list_values, start_pos, i, trend, is_shot)
                return trend
    if to_right and list_values[start_pos] > list_values[start_pos + 1]:
        for i in range(start_pos + 1, len(list_values)):
            if (list_values[i] > list_values[start_pos] and i not in anomaly_points) or (i == len(list_values) - 1) or \
                    is_shot[i]:
                make_inclined(list_values, start_pos, i, trend, is_shot)
                return trend
    if not to_right and list_values[start_pos - 1] < list_values[start_pos]:
        for i in range(start_pos - 1, 0, -1):
            if (list_values[i] > list_values[start_pos] and i not in anomaly_points) or (i == 1) or is_shot[i]:
                make_inclined(list_values, i, start_pos, trend, is_shot)
                return trend
    if not to_right and list_values[start_pos - 1] > list_values[start_pos]:
        for i in range(start_pos - 1, 0, -1):
            if (list_values[i] < list_values[start_pos] and i not in anomaly_points) or (i == 1) or is_shot[i]:
                make_inclined(list_values, i, start_pos, trend, is_shot)
                return trend


def search_not_anomaly_index(list_values, trend, anomaly_points):
    min_dif = -1
    min_index = len(list_values) // 2
    for i in range(1, len(list_values) - 1):
        if abs(list_values[i] - trend(i)) > min_dif and i not in anomaly_points:
            min_dif = abs(list_values[i] - trend(i))
            min_index = i
    return min_index


def delete_alone_anomaly(list_values, trend):
    if len(list_values) == 1:
        return list_values
    if list_values[0] < list_values[1]:
        list_values[0] = list_values[1] - trend[1]
    if list_values[-1] > list_values[-2]:
        list_values[-1] = list_values[-2] + trend[1]
    for i in range(1, len(list_values) - 1):
        if (list_values[i] > list_values[i + 1] and list_values[i] > list_values[i - 1]) or (
                list_values[i] < list_values[i + 1] and list_values[i] < list_values[i - 1]):
            list_values[i] = (list_values[i - 1] + list_values[i + 1]) / 2
    return list_values


def delete_lower(list_values):
    trend = draw_trend(list_values)
    for i in range(len(list_values) - 1):
        if list_values[i] < list_values[i + 1] and abs(list_values[i + 1] - trend(i + 1)) < abs(
                list_values[i] - trend(i)):
            list_values[i] = list_values[i + 1] - trend[1]


def filter_values(list_values):
    trend = draw_trend(list_values)
    if len(list_values) <= 2:
        return list_values, trend, set()
    greatest_dif = get_anomaly_val(list_values)
    anomaly_points = set()
    for i in range(len(list_values) - 1):
        if list_values[i + 1] > list_values[i] or (
                abs(list_values[i + 1] - list_values[i]) > greatest_dif > MIN_GREATEST_DIF):
            anomaly_points.add(i)
            anomaly_points.add(i + 1)
    is_shot = np.zeros(len(list_values), dtype=bool)
    not_anomaly_index = search_not_anomaly_index(list_values, trend, anomaly_points)
    for i in range(not_anomaly_index, len(list_values) - 1):
        if list_values[i] < list_values[i + 1] or (abs(list_values[i + 1] - list_values[i]) > greatest_dif and abs(
                list_values[i + 1] - list_values[i]) > MIN_GREATEST_DIF):
            shot_inclined(list_values, i, True, anomaly_points, trend, is_shot)
    for i in range(not_anomaly_index, 0, -1):
        if list_values[i - 1] < list_values[i] or (abs(list_values[i - 1] - list_values[i]) > greatest_dif and abs(
                list_values[i - 1] - list_values[i]) > MIN_GREATEST_DIF):
            shot_inclined(list_values, i, False, anomaly_points, trend, is_shot)
            shot_inclined(list_values, i - 1, True, anomaly_points, trend, is_shot)
    delete_lower(list_values)
    return delete_alone_anomaly(list_values, trend), trend, anomaly_points


def build_hierarchy(list_rivers):
    rivers_as_point = list()
    for river in list_rivers:
        river_node = Node(river)
        for other_river_node in rivers_as_point:
            if other_river_node.value.getPointStart() == river.getPointEnd():
                river_node.children.append(other_river_node)
                other_river_node.parent = river_node
            if other_river_node.value.getPointEnd() == river.getPointStart():
                other_river_node.children.append(river_node)
                river_node.parent = other_river_node
        rivers_as_point.append(river_node)
    return list(filter(lambda river: river.parent == None, rivers_as_point))  # return list of roots


class Node:
    def __init__(self, value):
        self.value = value
        self.parent = None
        self.children = list()


class RiverData:
    def __init__(self, points, heights, heights_raw_corr, positive_trend, id, anomaly_points):
        self.points = points
        self.heights = heights
        self.start_height_correct = heights[0]
        self.end_height_correct = heights[-1]
        self.first_start_height = heights[0]
        self.first_end_height = heights[-1]
        self.heights_raw_corr = heights_raw_corr
        self.positive_trend = positive_trend
        self.id = id
        self.anomaly_points = anomaly_points

    def getPointStart(self):
        return self.points[0]

    def getPointEnd(self):
        return self.points[-1]

    def getStartHeight(self):
        return self.heights[0]

    def getEndHeight(self):
        return self.heights[-1]

    def setStartHeightCorrect(self, start_height):
        self.heights[0] = start_height

    def setEndHeightCorrect(self, end_height):
        self.heights[-1] = end_height

    def scaleHeights(self):
        for i in range(1, len(self.heights) - 1):
            self.heights[i] -= self.first_end_height
            self.heights[i] *= (self.heights[0] - self.heights[-1]) / (self.first_start_height - self.first_end_height)
            self.heights[i] += self.heights[-1]

    def getHeights(self):
        return self.heights

    def getPoints(self):
        return self.points

    def __eq__(self, other):
        return self.points == other.points


def count_of_digits(number):
    c = 0
    number = abs(number)
    while number != 0:
        number //= 10
        c += 1
    return c


def remove_elements(point_list, deleted_points):
    for del_point in deleted_points:
        point_list.remove(del_point)


class LevelingRivers(QgsProcessingAlgorithm):
    INPUT_RIVERS = 'INPUT_RIVERS'
    INPUT_LAKES = 'INPUT_LAKES'
    OUTPUT_RIVERS_LINES = 'OUTPUT_RIVERS_LINES'
    OUTPUT_RIVERS_POINTS = 'OUTPUT_RIVERS_POINTS'
    RASTER = 'RASTER'
    STEP_SIZE = 'STEP_SIZE'

    rivers = list()
    lakesHeights = list()

    def __init__(self):
        super().__init__()
        self.rivers_points = set()

    def delete_duplicated_rivers(self):
        duplicated_rivers_indexes = set()
        for i in range(len(self.rivers)):
            for j in range(i + 1, len(self.rivers)):
                if self.rivers[i] == self.rivers[j]:
                    duplicated_rivers_indexes.add(j)
        new_rivers_list = list()
        for i in range(len(self.rivers)):
            if i not in duplicated_rivers_indexes:
                new_rivers_list.append(self.rivers[i])
        self.rivers = new_rivers_list

    @staticmethod
    def tr(string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return LevelingRivers()

    def name(self):
        return 'levelingrivers'

    def displayName(self):
        return self.tr('Rivers Levelig')

    def group(self):
        return self.tr('Water Leveling')

    def groupId(self):
        return 'levelingwater'

    def shortHelpString(self):
        return self.tr("This script levels the water levels")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_RIVERS,
                self.tr('Input layer with rivers'),
                [QgsProcessing.TypeVectorLine]
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LAKES,
                self.tr('Input layer with lakes'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.RASTER,
                self.tr('Raster layer to sample'),
                [QgsProcessing.TypeRaster]
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_RIVERS_LINES,
                self.tr('Output layer for river lines'),
                QgsProcessing.TypeVectorLine
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_RIVERS_POINTS,
                self.tr('Output layer for river points'),
                QgsProcessing.TypeVectorPoint
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.STEP_SIZE,
                self.tr('Sife of step by river'),
                type=QgsProcessingParameterNumber.Double
            )
        )

    def getSource(self, parameters, context, parameter_name):
        source = self.parameterAsSource(parameters, parameter_name, context)
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, parameter_name))
        return source

    def getSinkLines(self, parameters, context, source):
        sink_fields = QgsFields()
        sink_fields.append(QgsField('river_id', QVariant.Int, 'int'))
        sink, dest_id = self.parameterAsSink(parameters, self.OUTPUT_RIVERS_LINES, context, sink_fields,
                                             QgsWkbTypes.LineStringZ, source.sourceCrs())
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT_RIVERS_LINES))
        return sink, dest_id

    @staticmethod
    def createPointFields():
        fields = QgsFields()
        fields.append(QgsField('river_id', QVariant.Int, 'int'))
        fields.append(QgsField('point_id', QVariant.Int, 'int'))
        fields.append(QgsField('height_bef', QVariant.Double, 'double'))
        fields.append(QgsField('height_aft', QVariant.Double, 'double'))
        fields.append(QgsField('straight', QVariant.Bool, 'bool'))
        fields.append(QgsField('anomaly', QVariant.Bool, 'bool'))
        return fields

    def getSinkPoints(self, parameters, context, source):
        sink_fields = self.createPointFields()
        sink, dest_id = self.parameterAsSink(parameters, self.OUTPUT_RIVERS_POINTS, context, sink_fields,
                                             QgsWkbTypes.MultiPoint, source.sourceCrs())
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT_RIVERS_LINES))
        return sink, dest_id

    def getFilePointOutput(self, parameters, context):
        return self.parameterAsFileOutput(parameters, self.OUTPUT_RIVERS_POINTS, context)

    @staticmethod
    def writeRiverDataLines(heights, points, sink, index):
        pointsZ = list()
        for i, point in enumerate(points):
            pointsZ.append(QgsPoint(point))
            pointsZ[i].addZValue(float(heights[i]))
        river_fields = QgsFields()
        river_fields.append(QgsField('river_id', QVariant.Int, 'int'))
        river_line = QgsFeature(river_fields)
        river_line.setGeometry(QgsGeometry.fromPolyline(pointsZ))
        river_line['river_id'] = index
        sink.addFeature(river_line, QgsFeatureSink.FastInsert)

    @staticmethod
    def sample_raster(raster_data_provider, point):
        height, have_res = raster_data_provider.sample(point, 1)
        if have_res:
            return height
        else:
            return -1.

    @staticmethod
    def calc_coeff(count_of_points):
        return 10 ** max(3, count_of_digits(count_of_points))

    @staticmethod
    def is_correct_river(heights):
        trend = draw_trend(heights)
        if trend[1] > 0.001:
            return False
        for i in range(1, len(heights)):
            if heights[i - 1] - heights[i] < -0.01:
                return False
        return True

    def process_line_river(self, point_list, raster_data_provider, index):
        heights = []
        deleted_points = list()
        for point in point_list:
            self.rivers_points.add(point)
            height, have_res = raster_data_provider.sample(point, 1)
            if have_res:
                heights.append(height)
            else:
                deleted_points.append(point)
        remove_elements(point_list, deleted_points)
        trend = draw_trend(heights)
        m = len(heights)
        if m == 1:
            self.rivers.append(RiverData(point_list, heights, heights, trend[1] > 0.00001, index, set()))
            return
        elif m == 2 or m == 3:
            k_ = m - 1
        elif m == 0:
            return
        else:
            k_ = 3
        x = np.linspace(1, m, m)
        tck = interpolate.splrep(x, heights, k=k_, s=self.calc_coeff(m))
        heights_interpolate = interpolate.splev(x, tck, der=0)
        if not self.is_correct_river(heights_interpolate):
            heights, trend, anomaly_points = filter_values(heights)
            tck = interpolate.splrep(x, heights, k=k_, s=self.calc_coeff(m))
            heights_interpolate = interpolate.splev(x, tck, der=0)
            self.rivers.append(
                RiverData(point_list, heights_interpolate, heights, trend[1] > 0.00001, index, anomaly_points))
        self.rivers.append(RiverData(point_list, heights_interpolate, heights, trend[1] > 0.00001, index, set()))

    def process_river(self, river_line, raster_data_provider, index):
        if river_line.geometry().isMultipart():
            for point_list in river_line.geometry().asMultiPolyline():
                self.process_line_river(point_list, raster_data_provider, index)
        else:
            point_list = river_line.geometry().asPolyline()
            self.process_line_river(point_list, raster_data_provider, index)

    def changeHeightData(self, point, new_height, start_index):
        for river_data in self.rivers[start_index:]:
            if river_data.getPointStart() == point:
                river_data.setStartHeightCorrect(new_height)
            elif river_data.getPointEnd() == point:
                river_data.setEndHeightCorrect(new_height)

    def correctHeight(self, point, start_index):
        sum_heights = 0
        count_rivers = 0
        for j in range(start_index, len(self.rivers)):
            if point == self.rivers[j].getPointStart() and not self.rivers[j].positive_trend:
                count_rivers += 1
                sum_heights += self.rivers[j].getStartHeight()
            elif point == self.rivers[j].getPointEnd() and not self.rivers[j].positive_trend:
                count_rivers += 1
                sum_heights += self.rivers[j].getEndHeight()
        if count_rivers > 0:
            self.changeHeightData(point, sum_heights / count_rivers, start_index)

    def process_point_river(self, point, index):
        lake_height = self.getLakeHeight(point)
        if lake_height > 0:
            self.changeHeightData(point, lake_height, index)
        else:
            self.correctHeight(point, index)

    def correctRiversEnds(self, feedback):
        total = 25.0 / (len(self.rivers)) if len(self.rivers) else 0
        count_steps = 0
        processed_points = list()
        for i, river_data in enumerate(self.rivers):
            count_steps += 1
            if river_data.getPointStart() not in processed_points:
                self.process_point_river(river_data.getPointStart(), i)
                processed_points.append(river_data.getPointStart())
            if river_data.getPointEnd() not in processed_points:
                self.process_point_river(river_data.getPointEnd(), i)
                processed_points.append(river_data.getPointEnd())
            feedback.setProgress(25 + int(count_steps * total))

    def scaleRiversHeights(self, feedback):
        total = 25.0 / (len(self.rivers)) if len(self.rivers) else 0
        count_steps = 0
        for riverData in self.rivers:
            count_steps += 1
            riverData.scaleHeights()
            trend = draw_trend(riverData.heights)
            if trend[1] > 0.00001:
                riverData.setStartHeightCorrect(trend(0))
                riverData.setEndHeightCorrect(trend(0))
                riverData.scaleHeights()
            feedback.setProgress(50 + int(count_steps * total))

    def writeRiverDataPoints(self, river_data, sink, point_fields, index, raster_data_provider):
        for i, point in enumerate(river_data.getPoints()):
            point_feature = QgsFeature(point_fields)
            point_feature.setGeometry(QgsGeometry.fromPointXY(point))
            point_feature['river_id'] = int(index)
            point_feature['point_id'] = i
            point_feature['height_bef'] = float(self.sample_raster(raster_data_provider, point))
            point_feature['height_aft'] = float(river_data.getHeights()[i])
            point_feature['straight'] = bool(river_data.positive_trend)
            point_feature['anomaly'] = i in river_data.anomaly_points
            sink.addFeature(point_feature, QgsFeatureSink.FastInsert)

    def writeRiversData(self, sink_lines, sink_points, feedback, raster_data_provider):
        total = 25.0 / (len(self.rivers)) if len(self.rivers) else 0
        count_steps = 0
        point_fields = self.createPointFields()
        for river_data in self.rivers:
            count_steps += 1
            self.writeRiverDataLines(river_data.getHeights(), river_data.getPoints(), sink_lines, river_data.id)
            self.writeRiverDataPoints(river_data, sink_points, point_fields, river_data.id, raster_data_provider)
            feedback.setProgress(75 + int(count_steps * total))

    def getLakeHeight(self, point):
        for lake_data in self.lakesHeights:
            if point in lake_data[0]:
                return lake_data[1]
        return -1.

    @staticmethod
    def convertToSingle(parameters, context, feedback, parameter_name, output):
        objects_single = processing.run("native:multiparttosingleparts", {
            'INPUT': parameters[parameter_name], "OUTPUT": output}, context=context, feedback=feedback)["OUTPUT"]
        return objects_single

    def pointsAlongGeometry(self, parameters, context, feedback, parameter_name, interval):
        temp_file_name = create_temp_file()
        objects_single = self.convertToSingle(parameters, context, feedback, parameter_name, temp_file_name)
        object_along = processing.run("native:densifygeometriesgivenaninterval", {
            'INPUT': objects_single, "OUTPUT": "TEMPORARY_OUTPUT", "INTERVAL": interval},
                                      context=context, feedback=feedback)["OUTPUT"]
        return object_along

    @staticmethod
    def calc_raster_pixel_diagonal(raster):
        return (raster.rasterUnitsPerPixelX() ** 2 + raster.rasterUnitsPerPixelY() ** 2) ** 0.5

    def check_rivers(self, root_rivers, feedback):
        for root in root_rivers:
            for children in root.children:
                if root.value.getStartHeight() < children.value.getStartHeight():
                    feedback.pushInfo("Root less than children! " + str(root.value.getStartHeight()) + ", " + str(
                        children.value.getStartHeight()))
            if len(root.children) != 0:
                self.check_rivers(root.children, feedback)

    def read_lakes_height(self, parameters, context):
        lakes = self.getSource(parameters, context, self.INPUT_LAKES)
        lakes_features = lakes.getFeatures()
        for lake in lakes_features:
            if lake.hasGeometry():
                lake_points = list()
                if lake.geometry().isMultipart():
                    for point_lists in lake.geometry().asMultiPolygon():
                        for point_list in point_lists:
                            for point in point_list:
                                lake_points.append(point)
                else:
                    for point in lake.geometry().asPolygon():
                        lake_points.append(point)
                self.lakesHeights.append((lake_points, lake["height"]))

    def processAlgorithm(self, parameters, context, feedback):
        raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
        raster_data_provider = raster.dataProvider()
        step_size = self.parameterAsDouble(parameters, self.STEP_SIZE, context)
        rivers_layer = self.pointsAlongGeometry(parameters, context,
                                                feedback, self.INPUT_RIVERS,
                                                max(step_size, self.calc_raster_pixel_diagonal(raster)))
        if rivers_layer.sourceCrs() != raster.crs():
            raise QgsProcessingException("CRS of raster layer and CRS of polygon layer don't match")
        sink_lines, dest_lines_id = self.getSinkLines(parameters, context, rivers_layer)
        sink_points, dest_points_id = self.getSinkPoints(parameters, context, rivers_layer)
        rivers_lines = rivers_layer.getFeatures()
        total = 25.0 / (rivers_layer.featureCount()) if rivers_layer.featureCount() else 0
        count_steps = 0
        for river_line in rivers_lines:
            if feedback.isCanceled():
                break
            if river_line.hasGeometry():
                self.process_river(river_line, raster_data_provider, count_steps)
            count_steps += 1
            feedback.setProgress(int(count_steps * total))
        self.delete_duplicated_rivers()
        self.read_lakes_height(parameters, context)
        self.correctRiversEnds(feedback)
        self.scaleRiversHeights(feedback)
        root_rivers = build_hierarchy(self.rivers)
        self.check_rivers(root_rivers, feedback)
        self.writeRiversData(sink_lines, sink_points, feedback, raster_data_provider)
        return {self.OUTPUT_RIVERS_LINES: dest_lines_id}

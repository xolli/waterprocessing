# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterNumber,
                       QgsGeometry,
                       QgsRectangle,
                       QgsField)
import numpy as np
from math import ceil


class LevelingLakes(QgsProcessingAlgorithm):
    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    RASTER = 'RASTER'
    UNIT = "UNIT"
    DEGREES = "DEGREES "
    METERS = "METERS"
    BUFFER_SIZE_INTERNAL = "BUFFER_SIZE_INTERNAL"
    BUFFER_SIZE_EXTERNAL = "BUFFER_SIZE_EXTERNAL"

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return LevelingLakes()

    def name(self):
        return 'levelinglakes'

    def displayName(self):
        return self.tr('Lakes Leveling')

    def group(self):
        return self.tr('Water Leveling')

    def groupId(self):
        return 'levelingwater'

    def shortHelpString(self):
        return self.tr("This script levels the water levels")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer with lakes'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.RASTER,
                self.tr('Raster layer to sample'),
                [QgsProcessing.TypeRaster]
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer'),
                QgsProcessing.TypeVectorAnyGeometry
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.UNIT,
                "Distance unit",
                ["Degrees", "Meters"]
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.BUFFER_SIZE_INTERNAL,
                "Size of internal buffer (in meters)",
                QgsProcessingParameterNumber.Double,
                None,
                True,
                0
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.BUFFER_SIZE_EXTERNAL,
                "Size of external buffer (in meters)",
                QgsProcessingParameterNumber.Double,
                None,
                True,
                0
            )
        )

    def getInternalBuffer(self, parameters, context):
        unit = self.parameterAsEnum(parameters, self.UNIT, context)
        buffer_size = self.parameterAsDouble(parameters, self.BUFFER_SIZE_INTERNAL, context)
        if buffer_size < 1e-20:
            raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
            buffer_size = raster.rasterUnitsPerPixelX()
        return [-buffer_size / 40000000 * 360, -buffer_size][unit]

    def getExternalBuffer(self, parameters, context):
        unit = self.parameterAsEnum(parameters, self.UNIT, context)
        buffer_size = self.parameterAsDouble(parameters, self.BUFFER_SIZE_EXTERNAL, context)
        if buffer_size < 1e-20:
            raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
            buffer_size = raster.rasterUnitsPerPixelX()
        return [buffer_size / 40000000 * 360, buffer_size][unit]

    def getLakesSource(self, parameters, context):
        lakes_source = self.parameterAsSource(parameters, self.INPUT, context)
        if lakes_source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))
        return lakes_source

    @staticmethod
    def get_fields(source):
        fields = source.fields()
        fields.append(QgsField('height', QVariant.Double, 'double'))
        fields.append(QgsField('mean', QVariant.Double, 'double'))
        fields.append(QgsField('amount_points', QVariant.Int, 'int'))
        fields.append(QgsField('height_from_shore', QVariant.Bool, 'bool'))
        return fields

    def getSink(self, parameters, context, source, fields):
        sink, dest_id = self.parameterAsSink(parameters, self.OUTPUT, context, fields, source.wkbType(),
                                             source.sourceCrs())
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))
        return sink, dest_id

    @staticmethod
    def mostHighHist(arr):
        a = np.array(arr)
        hist, bins = np.histogram(a, bins='auto')
        index_max = hist.argmax()
        return (bins[index_max] + bins[index_max + 1]) / 2

    @staticmethod
    def getMovingAverage10(arr):
        if len(arr) == 0:
            return None
        n = int(ceil(len(arr) * 0.1))
        arr.sort()
        return np.convolve(arr, np.ones((n,)) / n, mode='valid')[0]

    @staticmethod
    def selectHeightInPolygon(polygon, raster):
        point = polygon.pointOnSurface()
        if point.isNull():
            return None, True, polygon
        new_polygon = polygon.difference(
            QgsGeometry.fromRect(QgsRectangle(point.asPoint().x() - 0.00002, point.asPoint().y() - 0.00002,
                                              point.asPoint().x() + 0.00002, point.asPoint().y() + 0.00002)))
        height, have_res = raster.dataProvider().sample(point.asPoint(), 1)
        if have_res:
            return height, False, new_polygon
        return None, False, new_polygon

    def shoreHeight(self, lake, parameters, context):
        shore = lake.geometry().buffer(self.getExternalBuffer(parameters, context) * 2, 5).difference(
            lake.geometry().buffer((self.getExternalBuffer(parameters, context)), 5))
        raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
        heights = []
        for i in range(100):
            height, short_is_empty, shore = self.selectHeightInPolygon(shore, raster)
            if short_is_empty:
                break
            if height is not None:
                heights.append(height)
        if len(heights) == 0:
            return None
        return self.getMovingAverage10(heights)

    @staticmethod
    def filterHeights(heights_lake, shore_height):
        return list(filter(lambda h: h < shore_height, heights_lake))

    def getParams(self, lake, parameters, context):
        lake_geometry = lake.geometry().buffer(self.getInternalBuffer(parameters, context), 5)
        raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
        heights = []
        for i in range(150):
            height, lake_is_empty, lake_geometry = self.selectHeightInPolygon(lake_geometry, raster)
            if lake_is_empty:
                break
            if height is not None:
                heights.append(height)
        shore_height = self.shoreHeight(lake, parameters, context)
        if shore_height is not None:
            filtered_heights = self.filterHeights(heights, shore_height)
        else:
            filtered_heights = heights
        if len(filtered_heights) == 0 and shore_height is not None:
            result_height = shore_height
        elif len(filtered_heights) == 0 and shore_height is None:
            return -1, -1, 0, False
        else:
            result_height = self.mostHighHist(filtered_heights)
        mean_all_heights = -1
        if len(heights) != 0:
            mean_all_heights = np.mean(heights)
        return result_height, mean_all_heights, len(filtered_heights), len(filtered_heights) == 0

    @staticmethod
    def addLakeToOutput(lake, sink, fields_lake, height, mean, amount_points, is_height_from_shore):
        old_values = dict()
        for source_field in lake.fields().names():
            old_values[source_field] = lake[source_field]
        lake.setFields(fields_lake)
        for source_field in old_values.keys():
            lake[source_field] = old_values[source_field]
        lake['height'] = float(height)
        lake['mean'] = float(mean)
        lake['amount_points'] = int(amount_points)
        lake['height_from_shore'] = is_height_from_shore
        sink.addFeature(lake, QgsFeatureSink.FastInsert)

    def processAlgorithm(self, parameters, context, feedback):
        lakes_source = self.getLakesSource(parameters, context)
        raster = self.parameterAsRasterLayer(parameters, self.RASTER, context)
        if lakes_source.sourceCrs() != raster.crs():
            raise QgsProcessingException("CRS of raster layer and CRS of polygon layer don't match")
        fields_lake = self.get_fields(lakes_source)
        sink, dest_id = self.getSink(parameters, context, lakes_source, fields_lake)
        lakes = lakes_source.getFeatures()
        total = 100.0 / (lakes_source.featureCount()) if lakes_source.featureCount() else 0
        count_steps = 0
        for lake in lakes:
            if feedback.isCanceled():
                break
            if lake.hasGeometry():
                height, mean, amount_points, is_height_from_shore = self.getParams(lake, parameters, context)
                self.addLakeToOutput(lake, sink, fields_lake, height, mean, amount_points, is_height_from_shore)
            count_steps += 1
            feedback.setProgress(int(count_steps * total))
        return {}

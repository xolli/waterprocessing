from qgis.core import QgsProcessingProvider
from .levelingLakes import LevelingLakes
from .levelingRivers import LevelingRivers
from .interpolateRivers import InterpolateRivers
from os.path import dirname
from qgis.PyQt.QtGui import QIcon


class Provider(QgsProcessingProvider):

    def loadAlgorithms(self, *args, **kwargs):
        self.addAlgorithm(LevelingLakes())
        self.addAlgorithm(LevelingRivers())
        self.addAlgorithm(InterpolateRivers())

    def id(self, *args, **kwargs):
        return 'waterplugins'

    def name(self, *args, **kwargs):
        return self.tr('Water Leveling')

    def icon(self):
        lake_icon = dirname(__file__) + '/icon.png'
        return QIcon(lake_icon)

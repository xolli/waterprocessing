def classFactory(iface):
    from .waterLevelingPlugin import WaterLevelingPlugin
    return WaterLevelingPlugin(iface)
